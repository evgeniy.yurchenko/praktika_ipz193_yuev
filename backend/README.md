1.create .env

2.set DB creds in .env

2.change bin/mercure if needed for your OS

3.composer install

4.bin/console doctrine:database:create

5.bin/console doctrine:schema:update --force

6.npm install

7.npm run build

8.symfony server:start

9.JWT_KEY=konshensx ADDR=localhost:3000 CORS_ALLOWED_ORIGINS=http://localhost:8000 ./bin/mercure

