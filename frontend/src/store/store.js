import Vue from 'vue'
import Vuex from 'vuex'
import CreatePersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    CreatePersistedState()
  ],
  state: {
    isAuthenticated: false,
    username: '',
    userId: '',
    cardNumber: '',
    balance: '',
    notification: {
      show: false,
      type: "error",
      message: ""
    },
  },
  mutations: {
    updateUserInfo(state, payload) {
      state.username = payload.username
      state.userId = payload.userId
      state.cardNumber = payload.cardNumber
      state.balance = payload.balance
    },
    updateIsAuthenticated(state, value) {
      state.isAuthenticated = value
    },
    updateNotification(state, payload) {
      state.notification = payload
    },
    //TODO success
    clearNotification(state) {
      state.notification = {
      }
    },
    updateCurdNumber(state, {cardNumber}) {
      state.cardNumber = cardNumber
    },
    updateBalance(state, {balance}) {
      state.balance = balance
    }
  },
  actions: {
    setUserInfo({ commit }, payload) {
      commit('updateUserInfo', payload)
    },
    setIsAuthenticated({ commit }, value) {
      commit('updateIsAuthenticated', value)
    },
    setNotification({ commit }, payload) {
      commit('updateNotification', payload)
    },
    setCardNumber({commit}, payload) {
      commit('updateCurdNumber', payload)
    },
    setBalance({commit}, payload) {
      commit('updateBalance', payload)
    }
  }
})
