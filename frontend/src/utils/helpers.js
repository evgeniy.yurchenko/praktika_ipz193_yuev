export const formatFileSize = (size) => {
  let i = -1
  if (size < 512) {
    return size + ' B'
  }
  const byteUnits = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  do {
    size = size / 1024
    i++
  } while (size >= 1024)
  let res = Math.max(size, 0.1)
  if ((res ^ 0) !== res) {
    res = res.toFixed(1)
  }
  return res + byteUnits[i]
}


export const secondsToHIS = (time, fmt = 'h:i:s', padded = true, round = 'round') => {
  let h = Math.floor.call(undefined, time / 3600)
  if (fmt !== 'i:s') {
    time -= h * 3600
  }
  let m = Math[round].call(undefined, time / 60)
  time -= m * 60
  let s = time
  if (!fmt.includes('s') && fmt.includes('h') && !(m % 60)) {
    h += m / 60
    m = 0
  }
  const pad = (val) => val < 10 ? '0' + val : val.toString()
  if (padded) {
    [h, m, s] = [h, m, s].map(pad)
  }
  if (fmt === 'hh im' && h === 0) {
    fmt = 'im'
  }
  return fmt.replace('h', h).replace('i', m).replace('s', s)
}
