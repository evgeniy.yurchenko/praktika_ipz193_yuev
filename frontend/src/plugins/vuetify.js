import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: false,
        themes: {
            light: {
                primary: '#2f4ea5',
                accent: '#264088',
                warning: '#f1d236',
                error: '#CD3023',
                success: '#5aa228',
                red: '#CD3023',
            },
        },
    },
});
