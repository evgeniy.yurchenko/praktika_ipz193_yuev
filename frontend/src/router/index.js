import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "../store/store"
import Home from '../views/Home.vue'
// import auth from "./middleware/auth";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: {
      name: 'dashboard'
    },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import( '../views/Login.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/about',
    name: 'about',
    component: () => import( '../views/About.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import( '../views/SignUp.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import( '../views/Dashboard.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/chat',
    name: 'chat',
    component: () => import( '../views/Support.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/success-signup',
    name: 'success-signup',
    component: () => import( '../views/SuccessSignUpPage.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/admin/chat',
    name: 'adminChat',
    component: () => import( '../views/Support.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '*',
    name: 'pageNotFound',
    component: () => import( '../views/PageNotFound.vue'),
    meta: {
      requiresAuth: true,
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((route) => route.meta?.requiresAuth)) {
    if (store.state.isAuthenticated) {
      next();
    } else {
      next("/login");
    }
  } else {
    next();
  }
});

export default router
